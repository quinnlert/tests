Test.Summary = "Test Trafficserver with default configurations starts with curl test"

Test.SkipUnless(Condition.HasProgram("curl","Curl need to be installed on sytem for this test to work"))


Test.MakeATSProcess("ts")
t = Test.AddTestRun("Test traffic server started properly")
t.StillRunningAfter = Test.Processes.ts

p = t.Processes.Default
p.Command = "curl http://127.0.0.1:8080"
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

