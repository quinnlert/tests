Test.Summary = "Test the Setup on Traffic Server Environment and Configuration Modifications"

Test.SkipUnless(Condition.HasProgram("curl",
                    "Curl needs to be installed on your system for this test to work"))

ts1 = Test.MakeATSProcess("ts1")
t = Test.AddTestRun("Test traffic server started properly")
t.StillRunningAfter = ts1

p = t.Processes.Default
p.Command = "curl 127.0.0.1:8080"
p.ReturnCode = 0

p.StartBefore(Test.Processes.ts1, ready = When.PortOpen(8080))
