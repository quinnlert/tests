Test.Summary = "Test filtering rules specified in ip_allow.config and remap.config"

Test.SkipUnless(Condition.HasProgram("curl","Curl need to be installed on system for this test to work"))

Test.MakeATSProcess("ts")

tr = Test.AddTestRun("Test Simple Allowed Connection")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET http://test0"
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

tr = Test.AddTestRun("Test Connection Deny")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET http://test1"
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

tr = Test.AddTestRun("Test Connection Deny")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET --interface eno1 http://test2" #change eno1 to specific machine
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

tr = Test.AddTestRun("Test Connection Deny")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET http://test3"
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

tr = Test.AddTestRun("Test Connection Deny")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET --interface eno1 http://test4" #change eno1 to specific machine
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

tr = Test.AddTestRun("Test Connection Allow")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET http://test5"
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

tr = Test.AddTestRun("Test Connection Allow")
tr.StillRunningAfter = Test.Processes.ts
p = tr.Processes.Default
p.Command = "curl -s --proxy localhost:8080 -D - -o /dev/null -X GET --interface eno1 http://test6" #change eno1 to specific machine
p.ReturnCode = 0
p.StartBefore(Test.Processes.ts, ready = When.PortOpen(8080))

# HTTP Status Code Only
# curl -s --proxy localhost:8080 -o /dev/null -w "%{http_code}" -X GET http://test0
