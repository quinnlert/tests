Test.Summary = '''
Testing IP Allow functionality
'''

Test.SkipUnless(Condition.HasProgram("curl","Curl need to be installed on system for this test to work"))
Test.testName = "IPAllow destination IP filtering check"

# For this test we get a reponse from the server if its IP in DNS lookup is not denied
server = Test.MakeOriginServer("server")
request_header = {"headers": "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n", "body": ""}
response_header = {"headers": "HTTP/1.1 200 OK\r\n", "body": ""}
server.addResponse("sessionfile.log", request_header, response_header)
dns = Test.MakeDNSServer("dns")
ts = Test.MakeATSProcess("ts")

# configurations for using uDNS and uWS with ts
ts.Disk.records_config.update({
    'proxy.config.dns.nameservers' : '127.0.0.1:{0}'.format(dns.Variables.Port),
    'proxy.config.dns.round_robin_nameservers' : 0,
    'proxy.config.http.cache.http' : 0,
    'proxy.config.reverse_proxy.enabled' : 0,
    'proxy.config.url_remap.pristine_host_hdr' : 1 # update/add to records.config
    })

# map all requests to uWS
ts.Disk.remap_config.AddLine(
    'regex_map http://(.*)/ http://127.0.0.1:{0}'.format(server.Variables.Port)
)

ts.Disk.ip_allow_config.AddLine({
    'dest_ip=128.0.0.1                                 action=ip_deny   method=GET',
    'dest_ip=128.0.0.0-128.255.255.255                 action=ip_allow  method=ALL',
    'dest_ip=34.35.166.23                              action=ip_allow  method=ALL',
    'dest_ip=129.0.0.1                                 action=ip_allow  method=ALL',
    'src_ip=127.0.0.1                                  action=ip_allow  method=ALL',
    })

# # No ip_allow.config -> all connections allowed.
# # Not implemented yet because it's broken. Need to fix this and then do the test.

# # Check an empty ip_allow.config
# ts.Disk.ip_allow_config.WriteOn('')

# tr=Test.AddTestRun()
# tr.Processes.Default.Command='curl -sS --proxy 127.0.0.1:{0} "http://www.example.com" --verbose'.format(ts.Variables.port)
# tr.Processes.Default.ReturnCode=52
# tr.Processes.Default.Streams.stderr="gold/deny.gold"
# tr.Processes.Default.StartBefore(server)
# tr.Processes.Default.StartBefore(Test.Processes.ts, ready=When.PortOpen(ts.Variables.port))

tr=Test.AddTestRun()
tr.Processes.Default.Command='curl -sS --proxy 127.0.0.1:{0} "http://www.example.com" --verbose'.format(ts.Variables.port)
tr.Processes.Default.ReturnCode=0

tr.Processes.Default.StartBefore(server)
tr.Processes.Default.StartBefore(Test.Processes.ts, ready=When.PortOpen(ts.Variables.port))
tr.Processes.Default.Streams.stderr="gold/https-200.gold"

