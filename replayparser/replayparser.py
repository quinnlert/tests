"""
This is a JSON file parser which store the 'body' object's raw data 
in a separate file and create a new body object of the path of where
the data is stored 
"""
from __future__ import absolute_import, division, print_function
from os import listdir
from os.path import isfile, join
import json
import binascii
import hashlib
from colorama import init, Fore, Back, Style

#give function a better name
def main( args ):
    #file parsing function
    input_dir = args.input_dir
    output_dir_json = args.output_dir
    output_dir_body = args.output_dir
    
    if not args.output_dir_json is None:
        output_dir_json = args.output_dir_json
    if not args.output_dir_body is None:
        output_dir_body = args.output_dir_body
        
    if args.filters is None:
        parse_all = True
    else:
        parse_all = False
    
    hash_table = {}
    
    for file in listdir(input_dir):
        input = join(input_dir, file)
        if isfile(input) and input.endswith('.json') and (parse_all or file in args.filters):          
            data = json.load(open(input))
                     
            for i in range(0, len(data["txns"])):
                try:
                    # didn't do' parsing for request because the body is usually empty
                    body_data = str(data["txns"][i]["response"]["body"])
                    # print(file)
                    # print(body_data)  
                except KeyError:
                    continue
                
                if not isinstance(body_data, dict):  
                    h = hashlib.md5()
                    h.update(body_data)
                    hv = h.hexdigest()
                        
                    try:
                        output_file_body = hash_table[hv]
                    except KeyError:                              
                        #output_file_body = join(output_dir_body, file[:len(file)-5] + "_" + str(i) + ".txt") 
                        output_file_body = join(output_dir_body, str(hv) + ".dat") ##figure out how hashlib randomly generates names  
                        f = open(output_file_body, "wb")
                        if args.decode_data is True:
                            try:
                                padding = 4 - len(body_data) % 4
                                body_data += "="*padding
                                f.write(body_data.decode("base64"))
                            except binascii.Error:
                                print(Fore.YELLOW + 'WARNING: ' + Fore.RESET + 'Incorrect Padding in ' + file + " Response index: " + str(i))
                                continue
                        else:
                            f.write(body_data.encode("utf-8"))
                        
                        hash_table[hv] = output_file_body     
                        f.close()
                        if args.verbose: print (Fore.CYAN + "Body string data written to " + output_file_body + Fore.RESET)
                    
                    body_obj = {"type": "file", "value": output_file_body} # TODO: We don't want to store absolute path
                    data["txns"][i]["response"]["body"] = body_obj
                    
            output_file_json = join(output_dir_json, file)    
            f = open(output_file_json, "wb")    
            f.write(json.dumps(data).encode("utf-8"))
            f.close()
            if args.verbose: print (Fore.GREEN + "New Format JSON written to " + output_file_json + Fore.RESET)   
                
                