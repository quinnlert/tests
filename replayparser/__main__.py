from __future__ import absolute_import, division, print_function
import os
from os.path import join, isfile, abspath, exists
import argparse
import replayparser 

def PathExists( args ): 
    #checks if directory path exists   
    path = abspath(args)
    if not exists(path):
        msg = '"{0}": No such directory'.format(path)
        raise argparse.ArgumentTypeError(msg)
    return str(path) 

def Path( args ): 
    #checks if the path name is a valid format  
    path = abspath(args)
    if not exists(path):
        os.makedirs(path)
    return str(path)

# def isOutputSet( args ):
#     #checks if there is output directory for both the json files and the body data     
#     if args.output_dir is None and (args.output_dir_json is None or args.output_dir_body is None):
#         msg = 'Please specify --output-dir or both --output-dir-json and --output-dir-body'
#         raise argparse.ArgumentTypeError(msg)    

def doFilesExist( input_dir, files ):
    #check if the specified files in --filters exist
    if files is None:
        return #nothing to check here
    
    if files==[]:
        msg = 'Please specify file(s) for --filters'
        raise argparse.ArgumentTypeError(msg)  

    for f in files:
        path = join(input_dir, f)
        if not exists(path):
            msg = '"{0}": No such file'.format(path)
            raise argparse.ArgumentTypeError(msg) 

def isOverwriting( input_dir, output_dir, output_dir_json, force ): #TODO: lambda functions for different cases
    #check if existing files will be overwritten
    if not force:
        for file in os.listdir(input_dir):
            path = join(input_dir, file)
            if isfile(path) and (output_dir_json is None and exists(join(output_dir, file)) or 
                    not output_dir_json is None and exists(join(output_dir_json, file))):   
                msg = '"{0}" exists in output directory. Use -f or --force to allow overwriting.'.format(file)
                raise argparse.ArgumentTypeError(msg)  

if __name__ == '__main__':
    #create primary command line parser
    parser = argparse.ArgumentParser()
    
    #-- Optional Arguments --
    parser.add_argument('-f', '--force', action='store_true', help='force input and output path to be the same')
    
    parser.add_argument('--input-dir', '--in-dir', 
                        nargs='?', 
                        default=abspath('.'),
                        type=PathExists, #TODO: check if there are files to read in this directory too
                        help='directory to read json files of replay data')
                        
    parser.add_argument('--output-dir', '--out-dir',
                        nargs='?',
                        default=abspath('./output'),
                        type=Path,
                        help='directory to write json files of replay data and string data of json response body')
    
    parser.add_argument('--output-dir-json', '--out-dir-json',
                        nargs='?',
                        type=Path,
                        help='directory to write json files of replay data')
    
    parser.add_argument('--output-dir-body', '--out-dir-body', 
                        nargs='?',
                        type=Path,
                        help='directory to write string data of json response body')
  
    parser.add_argument('--filters',
                        nargs='*',
                        help='filter file parsing by their names')
                        
    parser.add_argument('--decode-data', default=True, help='set to false to turn off decode data feature')                                  
    
    parser.add_argument('-V','--version', action='version', version='Version 0.1.Beta')

    #--- Console Options --
    parser.add_argument('--verbose', action='store_true', help='Display all verbose messages') 

    args = parser.parse_args()

    #doing checks before reading/modifying things
    doFilesExist(args.input_dir, args.filters)
    isOverwriting(args.input_dir, args.output_dir, args.output_dir_json, args.force)

    replayparser.main(args)
